const fs = require('fs')

const csv = require('csvtojson')
const matchesPath = "../data/matches .csv"
const deliveriesPath = "../data/deliveries.csv"


function numberOfMatchesPlayedPerYear(matchesArray) {
    const testArr = {};
    for (let i = 0; i < matchesArray.length; i++) {
        if (testArr.hasOwnProperty(matchesArray[i].season)) {
            testArr[matchesArray[i].season] += 1;
        }
        else {
            testArr[matchesArray[i].season] = 1;
        }
    }
    const fetchArray = [];
    for (let key in testArr) {
        let objectProperty = []
        objectProperty[0] = key
        objectProperty[1] = testArr[key]
        fetchArray.push(objectProperty)
    }

    return fetchArray
}





function numberOfMatchesWonPerTeamPerYear(matchesArray) {
    let team = [];
    let seasonRecord = {};
    for (let i = 0; i < matchesArray.length; i++) {
        if (seasonRecord.hasOwnProperty(matchesArray[i].season) || matchesArray[i].season === '') {
            continue;
        }
        else {
            seasonRecord[matchesArray[i].season] = fetchData(matchesArray, matchesArray[i].season)
        }
    }

    function fetchData(matchesArray, season) {
        let teamWins = {};
        for (let i = 0; i < matchesArray.length; i++) {
            if (matchesArray[i].season === season) {
                if (teamWins.hasOwnProperty(matchesArray[i].winner)) {
                    teamWins[matchesArray[i].winner]++;
                }
                else {
                    teamWins[matchesArray[i].winner] = 1;

                }
            }

        }
        return teamWins;
    }
    
    return seasonRecord
}





//read file matches.csv
csv()
    .fromFile(matchesPath)
    .then((matchesArray) => {
        //read file deliveries.csv
        csv()

            .fromFile(deliveriesPath)
            .then((deliveriesArray) => {

                //JSON.stringify() method will convert a JavaScript object or value to a JSON string.
                var json1 = JSON.stringify(numberOfMatchesPlayedPerYear(matchesArray));
                var json2 = JSON.stringify(numberOfMatchesWonPerTeamPerYear(matchesArray));
                
                //Write data in 'numberOfMatchesPlayedPerYear.json'  
                fs.writeFile('/home/sameer/ipl1/src/output/numberOfMatchesPlayedPerYear.json', json1, (err) => {

                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("Data saved to json file.")
                    }

                });

                //Write data in 'numberOfMatchesWonPerTeamPerYear.json'  
                fs.writeFile('/home/sameer/ipl1/src/output/numberOfMatchesWonPerTeamPerYear.json', json2, (err) => {
                    if (err) {
                        console.log(err)
                    }
                    else {
                        console.log("Data saved to json file.")
                    }
                });





                function extraRunsConcededPerTeamInTheYear2016(matchesArray, arr2) {
                    let matchID = [];
                    let teamName = [];
                    let extras = {};
                    for (let i = 0; i < matchesArray.length; i++) {

                        if ((matchesArray[i].season === '2016') && (matchID.includes(matchesArray[i].id) || matchesArray[i].season === '') && (teamName.includes(matchesArray[i].winner))) {
                            continue;
                        }
                        else {
                            if (matchesArray[i].season === '2016') {
                                matchID.push(matchesArray[i].id);            // Add all the ipl match id's of 2016 season into an array called matchID
                                teamName.push(matchesArray[i].winner);       // Add all the ipl team names of season 2016 into an array called teamName
                                extras[matchesArray[i].winner] = 0;          // Add all the team names into an object called extras with initial value of extra runs=0
                            }
                        }

                    }

                    for (let j = 0; j < deliveriesArray.length; j++) {
                        if (matchID.includes(deliveriesArray[j].match_id)) {

                            let a = parseInt(deliveriesArray[j].extra_runs)    //Type casting is done to convert the string value into number value 
                            extras[deliveriesArray[j].bowling_team] += a;
                        }
                    }

                    const fetchArray=[];
                    for(let key in extras)
                    {
                        let objectProperty=[]
                        objectProperty[0]=key
                        objectProperty[1]=extras[key]
                        fetchArray.push(objectProperty)
                    }

                    var json3 = JSON.stringify(fetchArray);

                    // Write data in 'extraRunsConcededPerTeamInTheYear2016.json'  
                    fs.writeFile('/home/sameer/ipl1/src/output/extraRunsConcededPerTeamInTheYear2016.json', json3, function (err) {
                        if (err) {
                            console.log(err)
                        }
                        else {
                            console.log("Data saved to json file.")
                        }

                    });
                }
                extraRunsConcededPerTeamInTheYear2016(matchesArray, deliveriesArray)





                function top10EconomicalBowlersInTheYear2015(matchesArray, deliveriesArray) {

                    let matchID = [];

                    for (let i = 0; i < matchesArray.length; i++) {
                        if (matchesArray[i].season === '2015') {

                            if (matchID.includes(matchesArray[i].id)) {
                                continue;
                            }
                            else {

                                matchID.push(matchesArray[i].id);

                            }
                        }
                    }

                    let obj = {};

                    for (let j = 0; j < deliveriesArray.length; j++) {
                        if (matchID.includes(deliveriesArray[j].match_id)) {
                            if (obj.hasOwnProperty(deliveriesArray[j].bowler)) {
                                continue;
                            }
                            else {
                                obj[deliveriesArray[j].bowler] = disp(deliveriesArray[j].bowler, j);
                            }

                        }
                    }

                    function disp(str, index) {
                        let runs = 0;
                        let balls = 0;
                        for (let j = index; j < deliveriesArray.length; j++) {
                            if (matchID.includes(deliveriesArray[j].match_id) && str === deliveriesArray[j].bowler) {

                                runs += parseInt(deliveriesArray[j].total_runs);
                                if (parseInt(deliveriesArray[j].wide_runs) === 0 && parseInt(deliveriesArray[j].noball_runs) === 0)
                                    balls++;

                            }
                        }

                        let economy = (runs / balls) * 6;
                        return economy;
                    }

                    let resultArr = [];                 
                    for (let key in obj) {              
                        let temp = []
                        temp[0] = key;
                        temp[1] = obj[key];
                        resultArr.push(temp);
                    }
                    resultArr.sort(function (a, b) { return a[1] - b[1] });
                    const save = []
                    for (let i = 0; i < 10; i++) {
                        save.push(resultArr[i])
                    }
                    var json4 = JSON.stringify(save);
                    //Write data in 'top10EconomicalBowlersInTheYear2015.json'  
                    fs.writeFile('/home/sameer/ipl1/src/output/top10EconomicalBowlersInTheYear2015.json', json4, function (err) {
                        if (err) {
                            console.log(err)
                        }
                        else {
                            console.log("Data saved to json file.")
                        }
                    });

                }

                top10EconomicalBowlersInTheYear2015(matchesArray, deliveriesArray)
            }).catch((err) => {
                console.log(err)
            });

    }).catch((err) => {
        console.log(err)
    });
